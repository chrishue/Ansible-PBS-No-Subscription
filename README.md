# PBS-No-Subscription

A role for Proxmox Backup Server, to set up the no-subscription repository

https://pbs.proxmox.com/docs/installation.html#proxmox-backup-no-subscription-repository

## Requirements

- Ansible has root access

## Install with ansible-galaxy

Add or append this to your requirements.yml file.
```yml
---
roles:
- src: git@codeberg.org:chrishue/PBS-No-Subscription.git
  scm: git
  name: chrishue.PBS-No-Subscription
```

```bash
ansible-galaxy install -r requirements.yml
```
> add `--force` to update.

## Usage example

```yml
---
- hosts: pbsnodes
  roles:
  - role: chrishue.PBS-No-Subscription
    dist: bullseye
    become: yes
```



